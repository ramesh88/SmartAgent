package com.yantranet.smartagent.presenter;

import android.content.Context;
import android.util.Log;

import com.yantranet.smartagent.business.AppBusiness;
import com.yantranet.smartagent.model.FetchConfigInfo;
import com.yantranet.smartagent.view.AppView;

import java.util.List;

/**
 * Created by Admin on 2/4/2018.
 */

public class AppPresenter extends BasePresenter implements AppPresenterListener {
    Context mContext;
    AppView mAppView;
    AppBusiness mAppBusiness;
    private static final String TAG = "Presenter";

    public AppPresenter(Context context, AppView appView) {
        mContext = context;
        mAppView = appView;
        Log.d(TAG, "Presenter");
        mAppBusiness = AppBusiness.getInstance(mContext);
        mAppBusiness.setAppPresenterListener(this);
    }

    public void getFetchCOnfig(int index) {
        displayProgressDialog("Please wait...!", "Fetching Configuration...");
        mAppBusiness.getAppList(index);

    }




    @Override
    public void onAppRegistrationSuccess(FetchConfigInfo responseData) {
        dismissProgressDialog();
        mAppView.onAppRegistrationSuccess(responseData);
    }

    @Override
    public void onAppRegistrationFailure(String error) {
        dismissProgressDialog();
        mAppView.onAppRegistrationFailure(error);
    }


}
