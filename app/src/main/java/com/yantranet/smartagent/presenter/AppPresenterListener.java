package com.yantranet.smartagent.presenter;


import com.yantranet.smartagent.model.FetchConfigInfo;

import java.util.List;

/**
 * Created by Admin on 2/4/2018.
 */

public interface AppPresenterListener {

    void onAppRegistrationSuccess(FetchConfigInfo responseData);
    void onAppRegistrationFailure(String error);

}
