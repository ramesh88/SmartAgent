package com.yantranet.smartagent.presenter;

import android.app.ProgressDialog;

import com.yantranet.smartagent.utils.Constants;


/**
 * Created by Admin on 2/4/2018.
 */

public class BasePresenter {
    ProgressDialog mProgressDialog;

    public void displayProgressDialog(String title,String message){
        mProgressDialog = new ProgressDialog(Constants.getmContext());
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }
    public void dismissProgressDialog(){
        mProgressDialog.dismiss();
    }

}
