package com.yantranet.smartagent;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.yantranet.smartagent.adapter.ConfigListAdapter;
import com.yantranet.smartagent.databaselayer.databasemanager.DatabaseMgr;
import com.yantranet.smartagent.model.Dependencies;
import com.yantranet.smartagent.model.FetchConfigInfo;
import com.yantranet.smartagent.presenter.AppPresenter;
import com.yantranet.smartagent.service.BackgroundService;
import com.yantranet.smartagent.utils.Constants;
import com.yantranet.smartagent.view.AppView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements AppView {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.tv_no_records)
    TextView tv_no_records;
    AppPresenter mAppPresenter;
    List<Dependencies> dependencies;
    int position = 0;
    DatabaseMgr databaseMgr;
    ProgressDialog mProgressDialog;
    ConfigListAdapter appListAdapter;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        startService(new Intent(this, BackgroundService.class));
        databaseMgr = new DatabaseMgr();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkPermission();
        } else {

            mAppPresenter = new AppPresenter(this, this);
            mAppPresenter.getFetchCOnfig(0);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
            }, 101);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allgranted = false;
        if (requestCode == 101) {
            //check if all permissions are granted

            for (int i = 0; i < grantResults.length; i++) {
                Log.d("granstresults...", String.valueOf(grantResults[i]));
                Log.d("permission...", String.valueOf(permissions[i]));
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;

                    break;
                }
            }
        }
        if (allgranted) {

            mAppPresenter = new AppPresenter(this, this);
            mAppPresenter.getFetchCOnfig(0);
        } else {
            requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
            }, 101);
        }


    }

    @Override
    public void onAppRegistrationSuccess(FetchConfigInfo responseData) {
        dependencies = responseData.getDependencies();
        if (dependencies == null || dependencies.size() == 0) {
            tv_no_records.setVisibility(View.VISIBLE);
            recycler_view.setVisibility(View.GONE);
        } else {
            appListAdapter = new ConfigListAdapter(MainActivity.this, dependencies, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            recycler_view.setLayoutManager(linearLayoutManager);
            recycler_view.setAdapter(appListAdapter);
            tv_no_records.setVisibility(View.GONE);
            recycler_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAppRegistrationFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        tv_no_records.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);

    }

    @Override
    public void onAction(int pos) {
        position = pos;
        String localPath = dependencies.get(position).getLocal_path();

        if (localPath != null && localPath.length() > 0) {
            showDialog(dependencies.get(position).getLocal_path(), dependencies.get(position).getType());
        } else {
            new DownloadFileFromURL().execute(dependencies.get(position).getCdn_path() + "^" +
                    dependencies.get(position).getName());
        }


    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            displayProgressDialog("Info...!", "Please wait downloading file...");
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            String split[] = f_url[0].split("\\^");
            File f = getOutputMediaFile(split[1]);
            try {

                URL url = new URL(split[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream());

                // Output stream to write file

                OutputStream output = new FileOutputStream(f.getPath());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
                Log.e("file Downloaded", f.getPath());

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return f.getPath();
        }


        @Override
        protected void onPostExecute(String file_url) {
            Log.e("file Downloaded in post", file_url);
            databaseMgr.updateLocalPath(file_url, dependencies.get(position).getId());
            dependencies = databaseMgr.retrieveCOnfigList();

            dismissProgressDialog();
            showDialog(file_url, dependencies.get(position).getType());

        }

    }

    public File getOutputMediaFile(String name) {

        // External sdcard location

        Boolean isSDPresent = android.os.Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
        File mediaStorageDir;
        if (isSDPresent) {

            mediaStorageDir = new File(Environment
                    .getExternalStorageDirectory().getPath()
                    + File.separator
                    + Constants.DIRECTORY_NAME);

        } else {
            mediaStorageDir = new File(this.getFilesDir().getPath()
                    + File.separator + Constants.DIRECTORY_NAME);


        }

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }

        // Create a media file name

        File mediaFile = null;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + name
        );
        return mediaFile;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.item_settings) {
            mAppPresenter.getFetchCOnfig(1);
            return true;
        }


        return super.onOptionsItemSelected(item);

    }

    public void showDialog(String title, String type) {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        View view = LayoutInflater.from(this).inflate(R.layout.web_view, null);
        dialog.setContentView(view);
        VideoView tv_logout_header = (VideoView) dialog.findViewById(R.id.web_view);
        tv_logout_header.setVideoPath(title);
        tv_logout_header.start();

        ImageView imageView = (ImageView) dialog.findViewById(R.id.iv_image);
        if (type.trim().equalsIgnoreCase("video")) {
            tv_logout_header.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
        } else {
            tv_logout_header.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            Glide.with(this).load(title).into(imageView);
        }
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                appListAdapter = new ConfigListAdapter(MainActivity.this, dependencies, MainActivity.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                recycler_view.setLayoutManager(linearLayoutManager);
                recycler_view.setAdapter(appListAdapter);
            }
        });

        dialog.show();
    }


    public void displayProgressDialog(String title, String message) {
        mProgressDialog = new ProgressDialog(Constants.getmContext());
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        mProgressDialog.dismiss();
    }
}
