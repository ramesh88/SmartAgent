package com.yantranet.smartagent.business;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.yantranet.smartagent.AppRegistration;
import com.yantranet.smartagent.databaselayer.databasemanager.DatabaseMgr;
import com.yantranet.smartagent.model.Dependencies;
import com.yantranet.smartagent.model.FetchConfigInfo;
import com.yantranet.smartagent.presenter.AppPresenterListener;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Admin on 2/4/2018.
 */

public class AppBusiness implements AppBusinessListener {
    private static AppBusiness instance = null;
    private static Context mContext;
    private static AppPresenterListener mAppPresenterListener;
    private static DatabaseMgr mDatabaseMgr;
    private static final String TAG = "AppBusiness";

    private AppBusiness() {

    }

    public static AppBusiness getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new AppBusiness();
            Log.d(TAG, "AppBusiness");
            mDatabaseMgr = DatabaseMgr.getInstance(mContext);
        }
        return instance;
    }

    public void setAppPresenterListener(AppPresenterListener appPresenterListener) {
        mAppPresenterListener = appPresenterListener;
    }


    public void getAppList(int index) {
        if (mDatabaseMgr.getAppListRecordCount() > 0 && index == 0) {
            Log.d(TAG, "From DB");
            FetchConfigInfo responseData = new FetchConfigInfo();
            responseData.setDependencies(mDatabaseMgr.retrieveCOnfigList());
            mAppPresenterListener.onAppRegistrationSuccess(responseData);
        } else {
            AppRegistration.getmInstance().callfecthConfig(this);
        }
    }


    @Override
    public void onSuccess(FetchConfigInfo responseData) {
        if (mDatabaseMgr.getAppListRecordCount() == 0) {
            Log.d(TAG, "From Service");
            List<Dependencies> app = responseData.getDependencies();
            mDatabaseMgr.insertCOnfigList(app);
        } else {
            List<Dependencies> app = responseData.getDependencies();
            mDatabaseMgr.updateCOnfigList(app);
            // update should be here
        }
        FetchConfigInfo responseData_db = new FetchConfigInfo();
        responseData_db.setDependencies(mDatabaseMgr.retrieveCOnfigList());
        mAppPresenterListener.onAppRegistrationSuccess(responseData);
        // mAppPresenterListener.onAppRegistrationSuccess(responseData);

    }


    @Override
    public void onFailure(String error) {
        mAppPresenterListener.onAppRegistrationFailure(error);
    }


}
