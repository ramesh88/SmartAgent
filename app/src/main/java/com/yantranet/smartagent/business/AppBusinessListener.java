package com.yantranet.smartagent.business;


import com.yantranet.smartagent.model.FetchConfigInfo;

/**
 * Created by Admin on 2/4/2018.
 */

public interface AppBusinessListener {
    public void onSuccess(FetchConfigInfo responseData);
    public void onFailure(String error);
}
