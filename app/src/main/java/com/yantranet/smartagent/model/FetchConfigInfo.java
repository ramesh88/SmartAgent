package com.yantranet.smartagent.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 2/3/2018.
 */

public class FetchConfigInfo {

    private List<Dependencies> dependencies;


    public List<Dependencies> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Dependencies> dependencies) {
        this.dependencies = dependencies;
    }
}
