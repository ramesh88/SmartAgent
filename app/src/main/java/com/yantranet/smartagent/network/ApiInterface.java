package com.yantranet.smartagent.network;

import com.yantranet.smartagent.model.FetchConfigInfo;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Admin on 2/4/2018.
 */

public interface ApiInterface {


    @GET("fetch_config")
    Call<FetchConfigInfo> fetchConfig();

}
