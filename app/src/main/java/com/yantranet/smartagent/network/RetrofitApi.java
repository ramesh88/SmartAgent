package com.yantranet.smartagent.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by SM331089 on 9/13/2017.
 */

public class RetrofitApi {

     private static Retrofit retrofit=null;
    private static final String BASE_URL= "https://demo6977317.mockable.io/";
    private static final long CONNECT_TIMEOUT = 60000;   // 2 seconds
    private static final long READ_TIMEOUT = 60000;      // 2 seconds


     public static Retrofit getRetrofit(){
         if(retrofit == null){
             retrofit = new Retrofit.Builder()
                     .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create())
                     .client(getOkHttpClient())
                     .build();
         }
         return retrofit;
     }

    public static OkHttpClient getOkHttpClient(){

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new OkHttpInterceptor()).build();
        return okHttpClient;


    }


}
