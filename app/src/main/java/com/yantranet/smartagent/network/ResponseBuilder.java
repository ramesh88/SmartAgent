package com.yantranet.smartagent.network;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Admin on 2/4/2018.
 */

public class ResponseBuilder {
    private static final String TAG="ResponseBuilder";

    public Response getResponseBuilder(int responseCode, String description, String json, Request request) {

        Response response = new Response.Builder().request(request).protocol(Protocol.HTTP_1_1)
                .code(responseCode).message(description)
                .body(ResponseBody.create(MediaType.parse("application/json"), json))
                .headers(request.headers())
                .build();
        return response;

    }
}
