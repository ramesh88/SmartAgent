package com.yantranet.smartagent.network;

import android.util.Log;

import com.google.gson.Gson;
import com.yantranet.smartagent.model.FetchConfigInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by SM331089 on 9/13/2017.
 */

public class ApiService {

    Retrofit retrofitApi;
    ApiInterface apiInterface;

    public ApiService(){
        retrofitApi=RetrofitApi.getRetrofit();
        apiInterface=retrofitApi.create(ApiInterface.class);
    }



    public void callfecthConfig(){
        Call<FetchConfigInfo> call=apiInterface.fetchConfig();
        call.enqueue(new Callback<FetchConfigInfo>() {
            @Override
            public void onResponse(Call<FetchConfigInfo> call, Response<FetchConfigInfo> response) {
                Log.d("Response Code :",""+response.code());
                String cloud_response=new Gson().toJson(response.body());
                Log.d("App Reg:",cloud_response);

            }
            @Override
            public void onFailure(Call<FetchConfigInfo> call, Throwable t) {

            }
        });
    }


}
