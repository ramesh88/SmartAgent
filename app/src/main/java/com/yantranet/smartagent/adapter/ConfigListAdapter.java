package com.yantranet.smartagent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yantranet.smartagent.R;
import com.yantranet.smartagent.model.Dependencies;
import com.yantranet.smartagent.view.AppView;

import java.util.List;

/**
 * Created by Admin on 2/4/2018.
 */

public class ConfigListAdapter extends RecyclerView.Adapter<ConfigListAdapter.MyViewHolder> {

    private List<Dependencies> appsList;
    Context context;
    AppView listener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title, tv_type, tv_size, tv_status;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_type = (TextView) view.findViewById(R.id.tv_type);
            tv_size = (TextView) view.findViewById(R.id.tv_size);
            tv_status = (TextView) view.findViewById(R.id.tv_status);
        }


        @Override
        public void onClick(View view) {

            listener.onAction(this.getLayoutPosition());
            //   Toast.makeText(context,"Position:"+this.getLayoutPosition(),Toast.LENGTH_SHORT).show();
        }
    }


    public ConfigListAdapter(Context context, List<Dependencies> appsList, AppView listener) {
        this.appsList = appsList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_config_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Dependencies app = appsList.get(position);
        holder.tv_title.setText(app.getName());
        holder.tv_type.setText("Type : " + app.getType());
        String zise = app.getSizeInBytes();
        if (app.getType().trim().equalsIgnoreCase("video")) {
            long MEGABYTE = 1024L * 1024L;

            if (zise.trim().length() > 0 && !zise.trim().equalsIgnoreCase("0")) {
                long b = Long.parseLong(zise) / MEGABYTE;
                zise = String.valueOf(b) + " MB";
            }
        } else {
            long MEGABYTE = 1024L;

            if (zise.trim().length() > 0 && !zise.trim().equalsIgnoreCase("0")) {
                long b = Long.parseLong(zise) / MEGABYTE;
                zise = String.valueOf(b) + " KB";
            }
        }

        if (app.getLocal_path() != null && app.getLocal_path().trim().length() > 0) {
            holder.tv_status.setText("Click to Open");
            holder.tv_status.setTextColor(Color.parseColor("#00ff00"));
        } else {
            holder.tv_status.setText("Click to Download");
            holder.tv_status.setTextColor(Color.parseColor("#ff0000"));
        }

        holder.tv_size.setText("Size : " + zise);

    }

    @Override
    public int getItemCount() {
        return appsList.size();
    }


    public interface Listener {
        void addAction(int position);
    }
}
