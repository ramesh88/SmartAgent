package com.yantranet.smartagent;

import android.util.Log;

import com.google.gson.Gson;
import com.yantranet.smartagent.network.ApiInterface;
import com.yantranet.smartagent.network.RetrofitApi;
import com.yantranet.smartagent.business.AppBusinessListener;
import com.yantranet.smartagent.model.FetchConfigInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Admin on 2/4/2018.
 */

public class AppRegistration {
    private static Retrofit retrofitApi;
    private static ApiInterface apiInterface;

    private static AppRegistration mInstance = null;

    private AppRegistration() {

    }

    public static AppRegistration getmInstance() {
        if (mInstance == null) {
            mInstance = new AppRegistration();
            retrofitApi = RetrofitApi.getRetrofit();
            apiInterface = retrofitApi.create(ApiInterface.class);
        }
        return mInstance;
    }

    public void callfecthConfig(final AppBusinessListener appBusinessListener) {

        Call<FetchConfigInfo> call = apiInterface.fetchConfig();
        call.enqueue(new Callback<FetchConfigInfo>() {
            @Override
            public void onResponse(Call<FetchConfigInfo> call, Response<FetchConfigInfo> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        //  if (appBusinessListener != null) {
                        appBusinessListener.onSuccess(response.body());
                        // }
                    } else {
                        appBusinessListener.onFailure("Failed in calling service " + response.code());
                    }

                } else {
                    appBusinessListener.onFailure("Failed in calling service " + response.code());
                }


            }

            @Override
            public void onFailure(Call<FetchConfigInfo> call, Throwable t) {
                Log.e("Response code:", t.getLocalizedMessage());
                if (appBusinessListener != null) {
                    appBusinessListener.onFailure(t.getLocalizedMessage());

                } else {
                    //sendBroadCast(3,"error");
                }

            }
        });

    }


}
