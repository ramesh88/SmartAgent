package com.yantranet.smartagent.databaselayer.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.yantranet.smartagent.databaselayer.contractor.Contractor;
import com.yantranet.smartagent.model.Dependencies;
import com.yantranet.smartagent.model.FetchConfigInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Admin on 2/3/2018.
 */

public class DatabaseMgr {

    public static DatabaseMgr mInstance;
    public static SQLiteDatabase mDB;
    public static Context mContext;
    private static final String TAG = "DatabaseMgr";

    public DatabaseMgr() {

    }

    public static DatabaseMgr getInstance(Context context) {
        mContext = context;
        if (mInstance == null) {
            mInstance = new DatabaseMgr();
            Log.d(TAG, "DatabaseMgr");
            createTable(context);
        }
        return mInstance;
    }

    public long getAppListRecordCount() {
        long appListCount = 0;
        Cursor cursor = mDB.query(Contractor.ConfigListTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null) {
            appListCount = cursor.getCount();
            cursor.close();
        }
        return appListCount;
    }

    public static void createTable(Context context) {
        mDB = DBHelper.getInstance(context).getReadableDatabase();
    }

    public long insertCOnfigList(List<Dependencies> apps) {

        long rowCount = 0;
        for (Dependencies app : apps) {
            ContentValues cv = new ContentValues();
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_ID, app.getId());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_NAME, app.getName());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_TYPE, app.getType());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_SIZE, app.getSizeInBytes());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_PATH, app.getCdn_path());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_LOCAL_PATH, "");
            try {
                rowCount = mDB.insert(Contractor.ConfigListTable.TABLE_NAME, null, cv);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rowCount;
    }

    public long updateCOnfigList(List<Dependencies> apps) {

        long rowCount = 0;
        mDB.delete(Contractor.ConfigListTable.TABLE_NAME, null, null);
        for (Dependencies app : apps) {
            ContentValues cv = new ContentValues();
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_ID, app.getId());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_NAME, app.getName());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_TYPE, app.getType());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_SIZE, app.getSizeInBytes());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_PATH, app.getCdn_path());
            cv.put(Contractor.ConfigListTable.COLUMN_NAME_LOCAL_PATH, "");
            try {
                rowCount = mDB.insert(Contractor.ConfigListTable.TABLE_NAME, null, cv);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rowCount;
    }

    public List<Dependencies> retrieveCOnfigList() {
        List<Dependencies> apps = new ArrayList<>();

        Cursor cursor = mDB.query(Contractor.ConfigListTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Dependencies app = new Dependencies();
                app.setId(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_ID)));
                app.setName(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_NAME)));
                app.setType(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_TYPE)));
                app.setSizeInBytes(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_SIZE)));
                app.setCdn_path(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_CONFIG_PATH)));
                app.setLocal_path(cursor.getString(cursor.getColumnIndex(Contractor.ConfigListTable.COLUMN_NAME_LOCAL_PATH)));
                apps.add(app);

            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return apps;
    }

    public boolean updateLocalPath(String localPath, String id) {
        Log.e("in update", localPath);
        Log.e("in update", id);
        ContentValues prCv = new ContentValues();
        prCv.put(Contractor.ConfigListTable.COLUMN_NAME_LOCAL_PATH, localPath);
        try {
            mDB.update(Contractor.ConfigListTable.TABLE_NAME, prCv, Contractor.ConfigListTable.COLUMN_NAME_CONFIG_ID + " = '" + id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


}
