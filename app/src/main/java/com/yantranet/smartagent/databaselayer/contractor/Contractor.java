package com.yantranet.smartagent.databaselayer.contractor;

/**
 * Created by Admin on 2/3/2018.
 */

public class Contractor {

    public static final String COLUMN_TYPE_TEXT = " TEXT";
    public static final String COLUMN_TYPE_INTEGER = " INTEGER";
    public static final String COMA_SEP = " ,";

    public static final String COLUMN_COMMON_ID = "_ID";


    public static class ConfigListTable {
        public static final String TABLE_NAME = "ConfigList";
        public static final String COLUMN_NAME_CONFIG_ID="COLUMN_NAME_CONFIG_ID";
        public static final String COLUMN_NAME_CONFIG_NAME = "COLUMN_NAME_CONFIG_NAME";
        public static final String COLUMN_NAME_CONFIG_TYPE = "COLUMN_NAME_APP_TYPE";
        public static final String COLUMN_NAME_CONFIG_SIZE = "COLUMN_NAME_CONFIG_SIZE";
        public static final String COLUMN_NAME_CONFIG_PATH = "COLUMN_NAME_CONFIG_PATH";
        public static final String COLUMN_NAME_LOCAL_PATH = "COLUMN_NAME_LOCAL_PATH";

        public static final String APP_LIST_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_CONFIG_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_CONFIG_NAME + COLUMN_TYPE_TEXT +COMA_SEP +
                COLUMN_NAME_CONFIG_TYPE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_CONFIG_SIZE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_CONFIG_PATH + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_LOCAL_PATH + COLUMN_TYPE_TEXT  + " )";
    }


}
