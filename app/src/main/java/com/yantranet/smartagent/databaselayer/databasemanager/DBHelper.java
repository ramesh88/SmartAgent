package com.yantranet.smartagent.databaselayer.databasemanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yantranet.smartagent.databaselayer.contractor.Contractor;

/**
 * Created by Admin on 2/3/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME="MDM";
    public static final int DB_VERSION=2;
    private static DBHelper instance;
    private static final String TAG="DBHelper";



    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(TAG,"Constructor");
    }

    public static DBHelper getInstance(Context context){
        if(instance == null){
            instance = new DBHelper(context);
            Log.d(TAG,"DBHelper");
        }
        return instance;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Contractor.ConfigListTable.APP_LIST_QUERY);
        Log.d(TAG,"Table Created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DELETE TABLE IF EXISTS "+Contractor.ConfigListTable.TABLE_NAME);
        onCreate(db);
    }
}
