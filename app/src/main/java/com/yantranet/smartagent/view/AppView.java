package com.yantranet.smartagent.view;

import android.location.Location;
import android.net.wifi.ScanResult;

import com.yantranet.smartagent.model.FetchConfigInfo;

import java.util.List;

/**
 * Created by Admin on 2/4/2018.
 */

public interface AppView {
    void onAppRegistrationSuccess(FetchConfigInfo responseData);
    void onAppRegistrationFailure(String error);
    void onAction(int position);

}
